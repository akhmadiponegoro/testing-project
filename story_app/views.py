from django.shortcuts import render,redirect
from story_app.forms import StatusForm
from story_app.models import StatusModel

def index(request):
	if request.method == "POST":
		form = StatusForm(request.POST)
		if form.is_valid():
			data = form.save()
			data.save()
			return redirect('/')
	else:
		form = StatusForm()

	statusOfObject = StatusModel.objects.all().order_by('-date')
	return render(request, 'index.html', {'form': form, 'status': statusOfObject})
